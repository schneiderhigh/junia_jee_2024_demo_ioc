package junia.demo.ioc.apps.javaversion;

import junia.demo.ioc.apps.javaversion.config.ApplicationConfig;
import junia.demo.ioc.domain.drawing.Drawing;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringApplicationLoadedWithJavaConfig {

    public static void main(String[] args) {
        try(AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class)){
            Drawing bean = context.getBean(Drawing.class);
            System.err.println(bean.getTotalPerimeter());
            System.err.println(bean.getTotalSurface());
        }
    }

}
