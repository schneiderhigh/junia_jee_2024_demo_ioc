package junia.demo.ioc.domain.biz;

import junia.demo.ioc.domain.ddd.BusinessService;

@BusinessService
public class ComponentA {

    private final ComponentB componentB;

    public ComponentA(ComponentB componentB) {
        this.componentB = componentB;
    }

    public ComponentB getComponentB() {
        return componentB;
    }
}
