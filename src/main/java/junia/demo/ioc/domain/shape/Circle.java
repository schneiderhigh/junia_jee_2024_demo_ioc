package junia.demo.ioc.domain.shape;

public class Circle implements Shape {

    private final double radius;


    public Circle(double radius) {
        this.radius = radius;
    }


    @Override
    public double getPerimeter() {
        return radius * Math.PI;
    }


    @Override
    public double getSurface() {
        return radius * radius * Math.PI;
    }


}
